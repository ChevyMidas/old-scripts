USE hzreport


DECLARE @Month INT

SET @Month = 2


--cmment

UPDATE A SET
	Actual = b.Actual
--SELECT *
FROM Leaderboard_Acquisitions a
INNER JOIN (
	SELECT 
		Affiliate,
		Actual
	FROM Leaderboard_Acquisitions
	WHERE Year = 2018
		AND Month = @Month
) as B
	ON a.Affiliate = b.Affiliate
		AND	a.year = 2018
		AND a.month <> @Month